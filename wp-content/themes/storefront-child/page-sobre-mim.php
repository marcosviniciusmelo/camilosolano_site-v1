<?php
get_header();
?>

<main class="sobre-page">
	
    <div class="main">
        <div class="left">
            <h2>Um pouco sobre mim</h2>
            <div class="text"><?php the_content() ?></div>

            <div class="redes">
				<a href="https://www.youtube.com/c/CamiloSolano" target="_blank"><i class="fab fa-youtube"></i></a>
				<a href="https://www.instagram.com/camilo.solano/" target="_blank"><i class="fab fa-instagram"></i></a>
				<a href="https://www.facebook.com/camilo.solano.52" target="_blank"><i class="fab fa-facebook"></i></a>
			</div>
            
        </div>
        <div class="right"><?php the_post_thumbnail('large'); ?></div>
    </div>
	
</main>	

<?php
get_footer();