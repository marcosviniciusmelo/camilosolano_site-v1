<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

// Variáveis para todo o tema
$nome_blog = get_bloginfo('name');
$link_blog = get_bloginfo('url');
$url_tema  = get_stylesheet_directory_uri() . '/';


/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

date_default_timezone_set('America/Sao_Paulo');



function remove_menu_items() {

    // remove_menu_page( 'index.php' ); //Painel
    // remove_menu_page('edit.php'); // Posts
    remove_menu_page('edit-comments.php'); // Comentarios

}
add_action( 'admin_menu', 'remove_menu_items' );

function sno_register_menus() 
{
    register_nav_menus(
        array(
            'nav-main' => 'Principal',
            'nav-footer' => 'Footer',
        )
    );
}

add_action('init', 'sno_register_menus');


// Função para exibir menu
function sno_show_menu($name_menu) 
{
	$output = wp_nav_menu(array(
		'theme_location' => $name_menu,
		'items_wrap' => '<ul class="listaMenu">%3$s</ul>',
		'echo' => 0,
		'link_before' => '',
		'link_after' => ''
	));
    return $output;
}

// Função para exibir menu
function my_acf_init() {

    // acf_update_setting('google_api_key', 'AIzaSyD98GTZK-eHP4wk0XGwM6E0S1Vf1nUwt3Q');
    
    if( function_exists('acf_add_options_page') ) {
        
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_slug'     => 'opcoes',
        ));
    }
}
add_action('acf/init', 'my_acf_init');



/*
* Slider
*/
function custom_slider() {
    $labels = array(
      'name'                => ( 'Sliders'),
      'singular_name'       => ( 'Slider'),
      );
    
    register_post_type( 'slider',
        array(
            'menu_icon'   => 'dashicons-admin-post',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title' ),
        )
    );
}
add_action('init', 'custom_slider');


/*
* Portfólio
*/
function custom_portfolio() {
    $labels = array(
      'name'                => ( 'Portfolio'),
      'singular_name'       => ( 'Portfolio'),
      );
    
    register_post_type( 'portfolio',
        array(
            'menu_icon'   => 'dashicons-admin-post',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor', 'thumbnail' ),
        )
    );
}
add_action('init', 'custom_portfolio');

/*
* Update number cart by ajax
*/
function updateNumberCart(){
    global $woocommerce;
    $count = $woocommerce->cart->cart_contents_count;

    echo $count;
    die();
}
add_action('wp_ajax_updateNumberCart', 'updateNumberCart');
add_action('wp_ajax_nopriv_updateNumberCart', 'updateNumberCart');


/**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query( $q ) {
    $tax_query = (array) $q->get( 'tax_query' );
    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'materiais', 'commission' ), // Don't display products in the clothing category on the shop page.
           'operator' => 'NOT IN'
    );
    $q->set( 'tax_query', $tax_query );
}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );  