<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */	
?>
<?php global $url_tema, $nome_blog, $link_blog, $api_url; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo $url_tema ?>assets/images/favicon.png" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css?<?php echo rand(000,999) ?>" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

<?php if(is_single()): ?>
	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f9fdd353b1cfe00120b72ed&product=inline-share-buttons" async="async"></script>
<?php endif; ?>

<?php if(is_page('meu-portfolio')): ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php endif; ?>

<script>
    var url = '<?php echo $link_blog ?>';
    var api_url = '<?php echo $api_url ?>';
</script>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">

	<header id="masthead" class="site-header">
		<div class="lineTop">
			<div class="main">
				<div class="redes">
					<a href="https://www.youtube.com/c/CamiloSolano" target="_blank"><i class="fab fa-youtube"></i></a>
					<a href="https://www.instagram.com/camilo.solano/" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="https://www.facebook.com/camilo.solano.52" target="_blank"><i class="fab fa-facebook"></i></a>
				</div>
			</div>
		</div>
		<div class="lineBottom">
			<div class="main">
				<h1 class="brand">
					<a href="<?php echo $link_blog ?>"><img src="<?php echo $url_tema ?>assets/images/brand.svg" alt="Camilo Solano"></a>
					<?php echo $nome_blog ?>
				</h1>
				<div class="right">
					<div class="menu"><?php echo sno_show_menu('nav-main') ?></div>
					<div class="icons">
						<div class="iconMenu"><img src="<?php echo $url_tema ?>assets/images/icones/menu.png"></div>
						<a href="javascript:;" class="icon clickSearch"><img src="<?php echo $url_tema ?>assets/images/icones/search.svg"></a>
						<a href="<?php echo $link_blog ?>/minha-conta" class="icon"><img src="<?php echo $url_tema ?>assets/images/icones/account.svg"></a>
						<a href="<?php echo $link_blog ?>/carrinho" class="icon linkCart">
							<div class="number"></div>
							<img src="<?php echo $url_tema ?>assets/images/icones/cart.svg" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<?php if( !is_front_page() && !is_page('contato') && !is_page('meu-portfolio') && !is_shop() && !is_product_category() && !is_page('blog') ): ?>
	<div id="content" class="site-content <?php echo (!is_shop()) ? 'inner-page' : 'produtos-page' ?>" tabindex="-1">
		<div class="col-full">
	<?php endif; ?>
		<?php do_action( 'storefront_content_top' );
