<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>
<?php global $url_tema, $nome_blog, $link_blog; ?>
<?php if( !is_front_page() && !is_page('contato') ): ?>
		</div><!-- .col-full -->
	</div><!-- #content -->
<?php endif; ?>

	<?php do_action( 'storefront_before_footer' ); ?>


	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->


<div class="modalBusca">
	<div class="close"><img src="<?php echo $url_tema ?>assets/images/icones/close.png"></div>
	<div class="bottom">
		<div class="main">
			<form action="<?php echo $link_blog ?>" method="GET">
				<input type="text" name="s" placeholder="O que você quer saber hoje?">
				<button class="btn btn-bordered" type="submit">Buscar</button>
			</form>
		</div>
	</div>
</div>
<div class="overModalBusca"></div>

<sectio class="newsletter">
	<div class="main">
		<div class="title">Cadastre-se para receber novidades</div>
		<?php echo do_shortcode('[mailpoet_form id="1"]') ?>
	</div>
</sectio>

<footer>

	<div class="main">
		<div class="lineTop">
			<div class="menu"><?php echo sno_show_menu('nav-footer') ?></div>
			<div class="redes">
				<a href="https://www.youtube.com/c/CamiloSolano" target="_blank"><i class="fab fa-youtube"></i></a>
				<a href="https://www.instagram.com/camilo.solano/" target="_blank"><i class="fab fa-instagram"></i></a>
				<a href="https://www.facebook.com/camilo.solano.52" target="_blank"><i class="fab fa-facebook"></i></a>
			</div>
		</div>
		<div class="lineBottom">
			<span>Todos os direitos reservados</span>
			<span>Camilo Solano | CNPJ: 27286425/0001-65</span>
			<a href="https://marcosviniciusmelo.com" target="_blank" class="linkMvm"><img src="<?php echo $url_tema ?>assets/images/mvm.png"></a>
		</div>
	</div>
	
</footer>

<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/slick.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/functions.js?<?php echo rand(000,9999) ?>"></script>

</body>
</html>
