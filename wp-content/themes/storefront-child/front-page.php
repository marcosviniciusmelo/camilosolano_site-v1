<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="home-page">
	
    <section class="sliderHome">
        <ul class="slider">

            <?php
                $argsS = array(
					'post_type'         => 'slider',
					'post_status'       => 'publish',
					'posts_per_page'    => 4,
					'orderby'   		=> 'menu_order',
					'order'     		=> 'ASC',
				);

                $queryS = new WP_Query($argsS);
				while ($queryS->have_posts()) : $queryS->the_post();

                echo '<li class="_item">
                        <div class="main">
                            <div class="left">
                                <h3>'.get_the_title().'</h3>
                                <h4>'.get_field('subtitulo').'</h4>
                                <div class="text">'.get_field('texto').'</div>
                                <a href="'.get_field('link').'" class="link">Ver mais</a>
                            </div>
                            <div class="right"><img src="'.get_field('imagem')['sizes']['medium'].'"></div>
                        </div>
                    </li>';

                endwhile;
            ?>
        </ul>
    </section>

    <section class="contentProdutos">
		<div class="main">
			<div class="top">
				<h3 class="title">Lançamentos</h3>
				<a href="<?php echo $link_blog; ?>/loja" class="linkMore">Ver todas ></a>
			</div>
			<?php echo do_shortcode('[recent_products columns=4 limit=4 category="materiais, commission" cat_operator="NOT IN"]'); ?>
		</div>
	</section>
    
    <?php $grid = get_field('grid', 'options'); ?>
    <section class="gridHome">
        <div class="main">
            <div class="collun">
                <a href="<?php echo $grid[0]['link'] ?>"><img src="<?php echo $grid[0]['imagem']['sizes']['large'] ?>"></a>
            </div>
            <div class="collun">
                <a href="<?php echo $grid[1]['link'] ?>"><img src="<?php echo $grid[1]['imagem']['sizes']['large'] ?>"></a>
                <a href="<?php echo $grid[2]['link'] ?>"><img src="<?php echo $grid[2]['imagem']['sizes']['large'] ?>"></a>
            </div>
            <div class="collun">
                <a href="<?php echo $grid[3]['link'] ?>"><img src="<?php echo $grid[3]['imagem']['sizes']['large'] ?>"></a>
            </div>
        </div>
    </section>
    
    <?php $destaque = get_field('bloco_de_destaque', 'options'); ?>
    <section class="bloco-destaque">
        <div class="main">
            <div class="left">
                <h3><?php echo $destaque['titulo'] ?></h3>
                <p><?php echo $destaque['texto'] ?></p>
                <a href="<?php echo $destaque['link'] ?>" class="btn"><?php echo $destaque['botao'] ?></a>
            </div>
            <figure class="person"><img src="<?php echo $destaque['imagem']['sizes']['large'] ?>"></figure>
        </div>
    </section>

    <section class="contentProdutos">
		<div class="main">
			<div class="top">
				<h3 class="title">Originais</h3>
				<a href="<?php echo $link_blog; ?>/categoria-produto/originais/" class="linkMore">Ver todas ></a>
			</div>
			<?php echo do_shortcode('[products columns=4 limit=4 category="originais"]'); ?>
		</div>
	</section>
    
    <?php $ads = get_field('ads', 'options'); ?>
    <div class="ads">
        <a href="<?php echo $ads['link'] ?>"><img src="<?php echo $ads['imagem']['sizes']['large'] ?>"></a>
    </div>

    <section class="contentProdutos">
		<div class="main">
			<div class="top">
				<h3 class="title">Quadrinhos</h3>
				<a href="<?php echo $link_blog; ?>/categoria-produto/quadrinhos/" class="linkMore">Ver todas ></a>
			</div>
			<?php echo do_shortcode('[products columns=4 limit=4 category="quadrinhos"]'); ?>
		</div>
	</section>

    <section class="contentProdutos materiais">
		<div class="main">
			<div class="top">
				<h3 class="title">Produtos PU</h3>
				<a href="<?php echo $link_blog; ?>/categoria-produto/materiais/" class="linkMore">Ver todas ></a>
			</div>
			<?php echo do_shortcode('[products columns=4 limit=4 category="materiais"]'); ?>
		</div>
	</section>

</main>	

<?php
get_footer();