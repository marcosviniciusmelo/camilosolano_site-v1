<div class="barShared">
	<div class="redes">
		<?php
			$url = urlencode( get_the_permalink() );
			$title = strip_tags( get_the_title() );
		?>
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&t=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-facebook-f"></i></a>
		<a href="https://twitter.com/share?url=<?php echo $url; ?>&text=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-twitter"></i></a>
		<a data-network="whatsapp" data-url="<?php echo get_the_permalink() ?>" class="st-custom-button"><i class="fab fa-whatsapp"></i></a>
		<a href="https://telegram.me/share/url?url=<?php echo get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram-plane"></i></a>

	</div>
</div>