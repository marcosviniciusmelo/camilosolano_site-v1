<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="contato-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$imagem = get_field('banner_top')['url'];
			?>
			<div class="title">FALE COMIGO</div>
		</div>

		<img src="<?php echo $imagem  ?>" alt="" class="bg">
	</section>

	<section class="ctnForm">

		<div class="main">
			<?php echo do_shortcode('[contact-form-7 id="91" title="Contato"]') ?>
		</div>

	</section>
		
</main>

<?php get_footer(); ?>