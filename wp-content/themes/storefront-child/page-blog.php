<?php
get_header();
?>

<main class="page-blog">
	
    <section class="bannerTop">
		<div class="main">
			<?php
				$imagem = get_field('banner_top')['url'];
			?>
			<div class="title">BLOG</div>
		</div>

		<img src="<?php echo $imagem  ?>" alt="" class="bg">
	</section>


	<section class="ctnPosts">
		<div class="main">
			<!-- <h3 class="title">Últimas</h3> -->
			<div class="posts col-2">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 2,
						'paged' 			=> $paged,
						'ignore_sticky_posts' => 1
					);
					$query = new WP_Query($args);
				?>
				<?php while ($query->have_posts()) : $query->the_post(); ?>
				<li class="_itemGrid style-02">
					<div class="ctn">
						<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
						<div class="bottom">
							<div class="left">
								<ul class="tags">
									<?php
										$ic=0;
										foreach ( get_the_terms($post->ID, 'category') as $key):
											if( $ic > 0 ) continue;

											$idCat = $key->term_id;
											if( $key->name != 'Crítica' && $key->parent != '' )
												$idCat = $key->parent;

											echo '<li><a style="background-color: '.get_field('cor', 'category_'.$idCat).';" href="'.get_the_permalink().'">'.get_cat_name( $idCat ).'</a></li>';
											$ic++;
										endforeach;
									?>
								</ul>
								<span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
								<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
							</div>
							<div class="right">
								<a href="<?php the_permalink($post->ID) ?>" class="btn btn-bordered">Leia mais</a>
							</div>
						</div>
						<figure class="bg">
							<?php 
								if( get_field('imagem_listagem') != '' ):
									echo '<img src="'.get_field('imagem_listagem')['url'].'" alt="">';
								else:
									the_post_thumbnail('medium');
								endif; 
							?>
						</figure>
					</div>
				</li>
				<?php endwhile; ?>
			</div>
		</div>	
	</section>

	<section class="ctnPosts">
		<div class="main">
			<div class="posts">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 100,
						'paged' 			=> $paged,
						'ignore_sticky_posts' => 1,
						// 'offset'			=> 2
					);
					$query = new WP_Query($args);
				?>
				<?php while ($query->have_posts()) : $query->the_post(); ?>
				<li class="_itemGrid">
					<div class="ctn">
						<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
						<div class="bottom">
							<ul class="tags">
								<?php
									$ic=0;
									foreach ( get_the_terms($post->ID, 'category') as $key):
										if( $ic > 0 ) continue;

										$idCat = $key->term_id;
										if( $key->name != 'Crítica' && $key->parent != '' )
											$idCat = $key->parent;

										echo '<li><a style="background-color: '.get_field('cor', 'category_'.$idCat).';" href="'.get_the_permalink().'">'.get_cat_name( $idCat ).'</a></li>';
										$ic++;
									endforeach;
								?>
							</ul>
							<span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
							<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
						</div>
						<figure class="bg">
							<?php 
								if( get_field('imagem_listagem') != '' ):
									echo '<img src="'.get_field('imagem_listagem')['url'].'" alt="">';
								else:
									the_post_thumbnail('medium');
								endif; 
							?>
						</figure>
					</div>
				</li>
				<?php endwhile; ?>
			</div>
		</div>	
	</section>
	
	
</main>	

<?php
get_footer();