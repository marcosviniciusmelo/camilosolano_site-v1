<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="portfolio-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$imagem = get_field('banner_top')['url'];
			?>
			<div class="title"><?php the_title()?></div>
		</div>

		<img src="<?php echo $imagem  ?>" alt="" class="bg">
	</section>

    <section class="grid">
        <div class="main">
            <?php
                $argsS = array(
                    'post_type'         => 'portfolio',
                    'post_status'       => 'publish',
                    'orderby'   		=> 'menu_order',
                    'order'     		=> 'ASC',
                    'posts_per_page'    => 100
                );

                $queryS = new WP_Query($argsS);
                while ($queryS->have_posts()) : $queryS->the_post();

                    echo '<div class="_item"><a data-fancybox="galeria" data-caption="'.get_the_content().'" href="'.get_the_post_thumbnail_url($post->ID, 'full').'"><img src="'.get_the_post_thumbnail_url($post->ID, 'medium').'"></a></div>';

                endwhile;
            ?>
        </div>
    </section>

		
</main>

<?php get_footer(); ?>