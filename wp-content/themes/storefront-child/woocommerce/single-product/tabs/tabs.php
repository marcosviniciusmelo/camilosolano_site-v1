<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */

global $post;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_content );
?>

<?php if( get_field('video') != '' || get_field('embed_video') != '' ): ?>
	<?php
		if( get_field('tipo_do_video') == 'Youtube' ){
			$video = get_field('video');
        	parse_str( parse_url( $video, PHP_URL_QUERY ), $my_array_of_vars );
			$embed = '<iframe width="100%" height="450" src="https://www.youtube.com/embed/'. $my_array_of_vars['v'] .'?autoplay=1&controls=0&fs=0&loop=0" frameborder="0" allowfullscreen></iframe>';
		}else{
			$embed = get_field('embed_video');
		}
        
    ?>
	</div></div></div>
	<section class="ctnVideo">
		<div class="main">
			<h3 class="title">Veja o processo</h3>
			<?php echo $embed; ?>
		</div>
	</section>
	<div class="col-full">
			<div class="woocommerce"></div>
			<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

<?php endif; ?>

<?php if ( $short_description ): ?>
<div class="ctnLine dadosTecnicos">
	<h4>Dados técnicos</h4>
	<div class="ctn">
		<div class="woocommerce-product-details__short-description">
			<?php echo $short_description; // WPCS: XSS ok. ?>
		</div>
	</div>
</div>	
<?php endif; ?>