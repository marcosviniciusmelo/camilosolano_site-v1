var $ = jQuery;

// SLICK //
$(".home-page .sliderHome .slider").slick({
  dots: true,
  infinite: true,
  speed: 1000,
  autoplay: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplaySpeed: 12000
});

$(window).on("resize load", function(){
  setTimeout(function(){
    if( $('.home-page .sliderHome .slider .slick-dots').length > 0 ){
      $('.home-page .sliderHome .slider .slick-dots').addClass('active');
    }
    $('.home-page .sliderHome .slider').addClass('active');
  }, 600);

});


$(".contentProdutos .products.columns-4, .related .products.columns-4").each(function(){

  $(this).slick({
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    responsive: [
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

});


 /*
  * Modal busca
  */
 $('.clickSearch').click(function(){
  $('.overModalBusca, .modalBusca').fadeIn();
});

$('.modalBusca .close, .overModalBusca').click(function(){
  $('.overModalBusca, .modalBusca').fadeOut();
});


$('#selectOrder').change(function(e){
  e.preventDefault();

  var url = $('#filterProduct').attr('action');
  var order = $(this).val();
  // var min_price = $('[name="min_price"]').val();
  // var max_price = $('[name="max_price"]').val();

  var paramers = [];
  // if( min_price != 0 ){ paramers.push('min_price='+min_price); }
  // if( max_price != $('[name="max_price"]').attr('data-max') ){ paramers.push('max_price='+max_price); }
  paramers.push('order='+order);

  var redirecto = url + '?' + paramers.join('&');
  window.location.href = redirecto;

});

$('.site-header .icons .iconMenu').click(function(){
  $('.site-header .menu').toggle();
})


/*
* Count carrinho
*/
updateNumberCart();

$( document.body ).on( 'added_to_cart', function(){
    updateNumberCart();
});

function updateNumberCart(){
  $('.linkCart .number').addClass('loading');
  $.ajax({
    type: "POST",
    url: url + "/wp-admin/admin-ajax.php",
    data: { action : 'updateNumberCart'},
    success: function(data) {
      console.log(data);
      $('.linkCart .number').text(data).removeClass('loading');
    }
  });
}

$('.single-product').each(function(){

  $('.product p.price').prependTo('.product form.cart')
  $('.single_add_to_cart_button').appendTo('.single-product div.product form.cart .quantity')

});